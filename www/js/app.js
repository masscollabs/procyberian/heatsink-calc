// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

    .run(function($ionicPlatform) {
	$ionicPlatform.ready(function() {
	    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
	    // for form inputs)
	    if(window.cordova && window.cordova.plugins.Keyboard) {
		cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
	    }
	    if(window.StatusBar) {
		StatusBar.styleDefault();
	    }
	    if (cordova.platformId == 'android') {
		StatusBar.backgroundColorByHexString("#388e3c");
	    }
	});
    })
    .controller('AppCtrl', function($scope, $ionicActionSheet, $ionicPopup) {
	$scope.formData = {
	    Tj: 150,
	    Rjc: 1.5,
	    P: 10,
	    Rch: 0.5,
	    Ta: 25,
	    Rha: 0
	}
	$scope.calculate = function () {
	    if ( parseFloat($scope.formData.Tj) > 175 ) { $scope.formData.Tj = 170 };
	    var Rha = (( parseFloat($scope.formData.Tj) - parseFloat($scope.formData.Ta) ) / parseFloat($scope.formData.P) ) - parseFloat($scope.formData.Rch) - parseFloat($scope.formData.Rjc);
	    $scope.formData.Rha = Math.round ( Rha * 100 ) / 100;
	}
	$scope.calculate();
	$scope.showSheet = function () {
	    var hideSheet = $ionicActionSheet.show({
		buttons: [
		    { text: 'Help' },
		    { text: 'About' }
		],
		buttonClicked: function (index) {
		    if (index == 0) {
			$ionicPopup.show({
			    templateUrl: 'help.html',
			    title: 'Help',
			    scope: $scope,
			    buttons: [
				{ text: 'Close' }
			    ]
			});
		    } else {
			$ionicPopup.show({
			    templateUrl: 'about.html',
			    title: 'About',
			    scope: $scope,
			    buttons: [
				{ text: 'Close' }
			    ]
			});
		    }
		    return true;
		}
	    });
	};
    })
